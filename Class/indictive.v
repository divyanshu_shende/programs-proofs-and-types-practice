Print True.
Print I.
Inductive myTrue : Prop := myTT.
Inductive myFalse : Prop :=.

Inductive myBool : Type :=
| mtrue : myBool
| mfalse:myBool
.

Inductive mBool : Set := mt | mf.
(*mt and mf are constants.*)

Inductive myNat : Set :=
| zero : myNat
| Su : myNat -> myNat
.

Check zero.
Check Su.
Check Su zero.

Inductive List (A : Type) : Type :=
| Nil : List A
| Cons: A -> List A -> List A
.

Inductive BTree : Type :=
| EmptyTree
| bTree : BTree -> BTree -> BTree
.

Set Implicit Arguments.

Inductive Vec (A:Type) : nat -> Type:=
| nil : Vec A 0
| cons {n : nat} : A -> Vec A n -> Vec A (S n)
.
(*Note that here the fact that length increases by one when you add an element is "built-in" and does not need separate proof.*)
Definition one: nat:= S 0.
                          
Definition x: Vec nat 1 := cons 1 (nil nat).

(*defining arguments for nil.*)
Arguments nil [A].

(*Make arguments implicit*)      
(*Inductive Sort (n : nat)(A : Type) : Vect A n -> Vect A n :=
.*)
(*In the above sort definition, we need not prove that sort does not change the length of the list. Although, the above type can be used for reverse too. It is generic. The body will specify action.*)

(*Check cons nat (S 1) (nil nat). *)
Check cons.  
Check one.
Fixpoint length {n : nat}{A : Type}(v : Vec A n) : nat :=
  match v with
  | nil => 0
  | cons _ vs => S(length vs)
  end.

(*Fixpoints can be made for inductive types too. Because
 otherwise, false will have a type. In type theory, inconsistency
 means that every type has an element.*)

(*underscore (_) means that match that value but don't use it.*)
Definition len {A : Type}{n : nat}(_:Vec A n) : nat := n.

Definition len2 {A :Type}{n : nat} : Vec A n -> nat := fun _ => n.

Check len.
Check len2.

(*Proof Assistant converts bugs in code to bugs in specification.*)

Fixpoint concat {A : Type}{n m : nat}(v : Vec A n)(v' : Vec A m) : (Vec A (n+m)) :=
  match v with
  | nil => v'
  | cons x vs => cons x (concat vs v')
  end.

Check Vec. 
Check concat.
Check cons.

(*Homework : Reverse*)

Fixpoint concatel {A : Type}{n : nat}(v : Vec A n) (e : A) : Vec A (S n):=
  match v with
  | nil => cons e nil
  | cons a vs => cons a (concatel vs a)
  end.
             

Fixpoint reverse {A : Type}{n : nat}(v : Vec A n) : Vec A n :=
  match v with
  | nil => nil
  | cons e vs => concatel (reverse vs) e
  end.

Check reverse.
Check nil.

Fixpoint reverse2 {A : Type}{n : nat}(v : Vec A n) : Vec A n :=
  match v with
  | nil => nil
  | cons e vs => concat (reverse2 vs) (cons e nil)
  end.
