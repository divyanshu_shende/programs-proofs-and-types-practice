(* A simple compiler from expression to stack machine instructions. An example from Cpdt *)

(* First we define the operators *)
Require Import List.
Open Scope list_scope.

(* The two binary operators that we suport *)
Inductive binop := Plus | Times.
Check binop.
(* The source language is the language of expression *)

Inductive expr :=
| Const : nat   -> expr
| Binop : binop -> expr -> expr -> expr.

(* The machine code *)

Inductive instr :=
| push : nat -> instr
| exec : binop -> instr.

Definition program := list instr.




(* Our compiler *)
Fixpoint compile (e : expr) : program :=
  match e with
    | Const n         => push n :: nil
    | Binop op e1 e2  => exec op :: (compile e1 ++ compile e2)
  end.
                                  


(* The meaning of stuff *)

Definition binopDenote (op : binop) : nat -> nat -> nat :=
  match op with
    | Plus  => plus
    | Times => mult
  end.

Fixpoint expDenote (e : expr) : nat :=
  match e with
    | Const n         => n
    | Binop op e1 e2  => binopDenote op (expDenote e1) (expDenote e2)
  end.


(* Now for the definition of the stack machine *)

Definition stack := list nat.


(* Meaning of stack machine *)

Definition instrDenote (i : instr)(s : stack) : option stack :=
  match i, s with
    | push n, _               => Some (n :: s)
    | exec op, (x :: y :: sp) => Some ((binopDenote op x y) :: sp)
    | _,_                       => None
  end.

Fixpoint programDenote (p : program) (s : stack) : option stack :=
  match p with
    | nil          => Some s
    | (i :: rest)
      => match instrDenote i s with
           | Some sp => programDenote rest sp
           | _       => None
         end
  end.

                      
(* The theorem we want to prove *)


Theorem compiler_is_correct : forall e : expr, programDenote (compile e) nil = Some (expDenote e :: nil).
Proof.
  intro e.  
  induction e.
  simpl. trivial.
Abort.

Lemma compiler_is_better : forall e p s, programDenote (compile e ++ p) s  = programDenote p (expDenote e :: s).
  intro e.
  induction e.
  simpl. trivial.
  intros.
  unfold compile.
  fold compile.
  unfold expDenote.
  fold expDenote.