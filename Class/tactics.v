(*From course repo for editing! *)
(*We can write our own tactice. We can use Ltac for the same.
Ltac --> Tactics Language.
Tactics are not trusted. In the sense that they need not be able to prove everything you want. 
Tactics are used to generate Gallina terms.
Goal is to simplpy "crush" proofs. For more details on this look at the textbook CPDT. General and works on all proofs.
Justification for crush -> Minor changes should not increase your work in re-proving the theorem.
Crush Working :
We give a proof for a theorem and then we see repeating patterns in proofs. Then we modify crush to capture this pattern.
*)
Inductive LE : nat -> nat -> Prop :=
| zLEn    {n   : nat} : LE 0 n
| snLEsm  {m n : nat} : LE n m -> LE (S n) (S m)
.

Check LE.
Hint Constructors LE.
Check LE.
Notation "x <= y" := (LE x y).
Notation "x < y"  := (LE (S x) y).
Notation "x >= y" := (LE y x).
Notation "x > y" :=  (LE (S y) x).
(*
  H:T
-------    assumption tactic.
   T

match in Ltac v/s match in Gallina.
*)
Ltac step :=
  match goal with
  | [ H : ?T |- ?T ] => exact H
  | [ |- _ -> _] => intro H; induction H
  end.
(*
repeat t : apply tactic t repeatedly till you stop making progress.
crush := 
    repeat (match goal with.... )
 *)
(*
Tactics can be written in OCaml as well.
*)
Ltac Crush_LE :=
  match goal with
    | [ |- 0 <= _ -> _        ]  => intro
    | [ |- 0 > _  -> _        ]  => assert False
  end.

Ltac crush_existential :=
  match goal with
    | [ n : nat |- exists k, 0 + k = ?n ] => exists n; trivial
    | [ H : exists _ : nat, ?n + _ = ?m  |- exists _:nat,S ?n + _ = S ?m ]
      => destruct H as [k HP]; exists k; simpl; auto
    | _ => idtac
  end.


Ltac crush_function_goal :=
  let H := fresh "H" in
  match goal with
    | [ |- _ -> False       ]   => intro H; inversion H
    | [ |- _ -> _           ]   => intro H; induction H
  end.


Ltac intro_a tac :=
  let H := fresh "H" in intro H ;  tac H.


Ltac intro_and_induction :=
  let H := fresh "H" in intro H ;  induction H.


Ltac crush :=
  repeat match goal with
           | [ _ : False |- _      ]   => contradiction
           | [ H : ?T |- ?T        ]   => exact H
           | [ |-  _ <= _          ]   => Crush_LE
           | [ |- exists _,_       ]   => crush_existential
           | [ |- _ -> _           ]   => crush_function_goal
           | _                         => eauto
          (* | _                         => trivial; autorewrite with core*)
         end.

Lemma refLE : forall (x : nat),  x <= x.
Proof.
  Unset Ltac Debug.
  crush.
  Show Proof.
Qed.

Check refLE.

Lemma positive_diplacement (x : nat)(y : nat): x <= y  -> exists k : nat, x + k = y.
Proof.
  crush.
Qed.

Lemma zgtxfalse : forall {x : nat},  0 > x -> False.
Proof.
  crush.
Qed.


Lemma nlen : forall (n : nat), n >= n.
Proof.
  crush.
Qed.

(*
Lemma antisym : forall (x y : nat), x <= y -> y <= x -> x = y.
Proof.
  Print Hint *.
  intro x. induction x.
  intro y. induction y.
  crush.
  intro. intro. assert False by exact (zgtxfalse H0). contradiction.
  induction y. intro. assert False by exact (zgtxfalse H). contradiction.
  intro H0. induction H0. intro H1. inversion H1. trivial.

*)