(*
Basics of Dependent Types.
Capture higher order logics. (allowing us to talk about predicates)
Types <--> Statements
Values <--> Proofs
Predicate Calculus : P(n) <--> Types. 
For predicates we use type families : Takes arguments and gives types.
In Coq, we have type universes Type0, Type1, etc. (even though they cannot be directly accessed in Coq).
Heterogenous List : [2, "hello"]
In dependent typed language, we can create something like heterogenous list. We need Pointed Types.
So the above list can be represented as [(Int, 2), (String, "hello")]
Type0 is a value of Type1, Type1 is a value of Type2, etc.
Any universe is a value in the next universe thereby giving a hierarchy.
forall(x:nat), P(x) -----> As a math statement, this should be a type.
So, we should be able to give a function that takes a proof of x in nat and should give a proof of P(x).
\pi_{x : A} P(x) -----------> Pi-Type : Pi-Type is the type of all such functions.
So, if f: forall(x:A) P(x) then, f(x) : P(x).
Example, constuctuct a function that takes n and gives a list with n one's in it.
f -> (n : nat) -> Vec nat n
Another example,
The element 1 in Z/nZ is dependent on n and therefore 1 can be thought of as a function of n.
Functions whose result type is dependent on input are quite natural.
We need to modify one of the rules as:

f:\pi_{x:A} B(x)   e:a
--------------------------
      f e : B(e)


Now for exists (there exists).
Here we have the sigma type.
\sigma_{x:A} B(x) : {(a,b) | b : B(a)}  Here 'a' is a witness and b is a proof that 'a' is a valid witness.

In Coq, we can define sigma types as follows.
 *)
Require Import List.
Require Import String.
Inductive sigmaT (A : Type) (B : A -> Type) : Prop :=
| exist (x:A) : B x -> sigmaT A B
.

Check sigmaT.
Check exist.

Inductive Pointed : Type :=
| inject {A : Type} : A -> Pointed.

Check Pointed.
Check inject.

(*Definition foo : list Pointed := [inject 2; inject 23].*)
  
(*
Coq -> Cumulative Universe. (Type 0 is Set)
Agda -> Non-cumulative Universe. But supports universal polymorphism.
 *)

(*
1) Cumulative Universe?
2) Universal Polymorphism?
3) Typical Ambiguity?
 *)

(*
Idris -> Has 1) and probably 3) but not 2).
Girard's Paradox : Inconsistency in the sense that every type has an inhabitant. So you can prove A and not(A) both. There is a simaliarity to Russels' Paradox. Therefore, infinity on both sides is problematic. Thus, we begin with Set.
 *)

(*
Coq Program -> Proofs + Extraction.
Extraction of code in a language. But proofs are lost in extracted code. So Coq gives us a way to separate the two.
Prop
 *)

Check sigmaT.
Check exist.

Check sigT.
Definition isHex (n:nat) : Prop := n <= 15.
Check isHex 2.
Check isHex.

Definition HexDigit : Type := {x:nat | isHex x}.
Check HexDigit.
Check sigmaT. 
Check sigmaT nat isHex.
Check sigT isHex.
Check sigT.

(*
write a function to throw away the proof in sigT and return x.
*)
Check sigT.
Print sigT.
Section Ex.
  Parameter A:Set.
  Parameter P:A->Prop.
  Definition Proj1  (u : @sigT A P) : A :=
    match u with
    | existT _ x y => x
    end.
  Print existT.
  Print ex_intro.
  (* The definition below will not compile.
  Definition Proj2 (u : exists x:A, P x) : A :=
    match u with
    | ex_intro _ x y => x
    end.
   *)
  Check Proj1.
  Print Proj1.
  Check Prop.
  Check Type.
  Check Set.
End Ex.

Set Printing Universes.

Check Set.
Check Prop.
Check Type.