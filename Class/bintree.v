Require Import Arith String.
Print True.

(*Red Black Tree*)

Inductive Colour := Red | Black.
(*Section allows us to define arguments and use them. Kind of like a scoping thing*)
(*Black Height and Root Color need to be taken care of.*)
Section RBTree.
  Parameter A : Type.
  (*Takes color of root and black height as argument.*)
  Inductive RBT : Colour -> nat -> Type:=
  | null : RBT Black 0
  | red {n : nat} : RBT Black n -> A -> RBT Black n -> RBT Red n
  | black {n : nat}{c1 c2 : Colour} : RBT c1 n -> A -> RBT c2 n -> RBT Black (S n)
  .
  (*Look at how the invariants of RBT are encoded in the definition above*)
  Check RBT Black 0.
  Check null.
  Definition x {a : A} : RBT Red 0 := red null a null.
  Check x.
  (*Define a function to count the number of nodes in tree. And prove that this number is between 2^n and 2^(n+1) where n is Black Height*)
End RBTree.

(*Prop Type cannot be matched! Keep this in mind*)
Check Prop.
(*Show that x<=y and y<=z --> x<=z*)

Inductive le : nat -> nat -> Prop := (*Note that le is a type, not a function*)
| zLEn {n : nat} : le 0 n
| mLESn {m n: nat} : le m n -> le (S m) (S n)
.

Check le.
Check zLEn.
Check mLESn.

Set Implicit Arguments.
(*Note that le 5 2 is still a type! But you cannot create a TERM of that type.*)



Lemma zleqn : forall (n:nat), le 0 n.
Proof.
  intros. 
  exact zLEn.
  Show Proof.
Qed.

Check zleqn.
  
(*Exercise.. finish!*)
Inductive le2 : nat -> nat -> Prop :=
| nLEn {n : nat} : le2 n n
| mLE2Sn {m n: nat} : le2 m n -> le2 m (S n)
. 

Check nat.
Check S.
Lemma nleqn : forall (n:nat), le n n.
Proof.
  induction n.
  exact (@zLEn 0).
 
  exact (mLESn IHn).
Qed.

Theorem succless :forall (m n : nat),  le m n -> le m (S n).
Proof.
  intros; induction m.
  exact (@zleqn (S n)).

  
Qed.

Check nleqn.
Check (fun n : nat => zLEn).
(*Prove this lemma.*)
Lemma transitive : forall (x y z : nat), le x y /\ le y z -> le x z.
Proof.
  intros.
  exact (nleqn x0).
  exact (nleqn y).
Qed.

(*
Fixpoint pred (n : nat) : 1 <= n -> nat.
The above def of predecessor function makes sure that it is defined for numbers that are greater than or equal to 1.
This is because 1<=n is a type which is actually le 1 n. Incase n is 0, we do not have an element of that type.

Existential Type.
Definition Hex := {x : nat | x <= 15}
 *)

Check Prop.