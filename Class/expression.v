Require Import List.

Inductive binop := Plus | Mult.

Inductive exp :=
| Const : nat -> exp
| Binop : binop -> exp -> exp -> exp.

Check exp.
Check Const.
Check Binop.

Inductive instr :=
| Push : nat -> instr
| Apply : binop -> instr.

Check Apply.

Definition program := list instr.

Check program.

Fixpoint compile (e:exp) : program :=
  match e with
  | Const n => Push n :: nil (*The double colon "::" is the list constructor.*)
  | Binop op e1 e2 => Apply op :: (compile e1 ++ compile e2)
  end.
 
(*
At this point we have a decent compiler. Now we need to show that it's correct.
To prove correctness, we need to model the machine on which it is running.
Also we need to give meaning to various things.
 *)

Check binop.
Definition binopDenote (op : binop) : nat -> nat -> nat :=
  match op with
  | Plus => plus
  | Mult => mult
  end.

(*
Note that in the above, "plus" and "mult" are Coq functions. Therefore, we have now given meaning to the binops.
 *)
Check Plus.

Fixpoint expDenote (e:exp) : nat :=
  match e with
  | Const n => n
  | Binop op e1 e2 => binopDenote op (expDenote e1) (expDenote e2)
  end.

(*As of now, we have given Coq meanings to exp and binop.*)

Definition Stack := list nat.

Definition instrDenote (i : instr) (S : Stack) : option Stack :=
  match i,S with
  | Push n, _ => Some (n::S)
  | Apply op, (x::y::sp) => Some ((binopDenote op x y) :: sp)
  | _, _ => None
  end.

Check binopDenote Plus 4 5.

Fixpoint programDenote (p : program) (S : option Stack) : option Stack :=
  match p,S with
  | (ins::ps), Some a => (programDenote ps (instrDenote ins a))
  | Nil, _ => None
  end.

Theorem compiler_is_correct : forall e:exp,
    programDenote (compile e) (Some nil) = Some (expDenote e::nil).
Proof.
  intros.