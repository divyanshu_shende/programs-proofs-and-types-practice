Require Import Arith String.

Inductive exp : Set :=
| Constant : nat -> exp
| Plus : exp -> exp -> exp
| Times : exp -> exp -> exp.

Fixpoint commuter (e : exp) : exp :=
  match e with
   | Constant _ => e
   | Plus e1 e2 => Plus (commuter e2) (commuter e1)
   | Times e1 e2 => Times (commuter e2) (commuter e1)
  end.


Inductive natural : Set :=
| Zero : natural
| Succ : natural -> natural.

Fixpoint add (n m: natural) : natural :=
  match n with
   | Zero => m
   | Succ n' => Succ (add n' m)
  end.

Theorem add_assoc : forall n m o,
    add (add n m) o = add n (add m o).
Proof.
  induction n.
  
  intros.
  simpl.
  reflexivity.
  Show Proof.

  intros.
  simpl.
  rewrite IHn.
  reflexivity.
  Show Proof.
Qed.

Lemma add_Succ : forall n m,
    add n (Succ m) = Succ (add n m).
Proof.
  induction n; simpl.
  intros.
  reflexivity.

  intros.
  rewrite IHn.
  reflexivity.
Qed.

Lemma add_Zero : forall n,
    add n Zero = n.
Proof.
  induction n; simpl.

  reflexivity.

  rewrite IHn.
  reflexivity.
  Show Proof.
Qed.

Theorem add_comm : forall n m, add n m = add m n.
Proof.
  induction n; intros; simpl.

  rewrite add_Zero.
  reflexivity.
  
  rewrite IHn.
  rewrite add_Succ.
  reflexivity.
Qed.
