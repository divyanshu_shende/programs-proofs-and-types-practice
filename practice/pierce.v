Definition pierce := forall (p q : Prop), ((p -> q) -> p) -> p.
Definition lem := forall (p : Prop), p \/ ~p.

Theorem pierce_implies_lem: pierce -> lem.
Proof.
  unfold pierce.
  unfold lem.
  intros.
Abort.
